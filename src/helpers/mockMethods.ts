/**
 * Mocks all methods of an object using jest.fn()
 * @param obj
 * @returns {T}
 */
export function mockMethods<T extends { [name: string]: any }>(obj: T): T {
    const mocked: T = Object.assign({}, obj);

    Object.keys(obj)
        .forEach(name => typeof mocked[name] === 'function' ? mocked[name] = jest.fn() : null)

    return mocked;
}

export default mockMethods;