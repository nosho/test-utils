import * as React from "react";

/**
 * Returns the original version of a React Component decorated with the @connect react-redux decorator,
 * this is useful for testing.
 * @param target
 * @returns {T}
 */
export function disconnect<T extends React.ComponentClass<any>>(target: T): T {
    if (!target) throw new Error(`Can't disconnect component, no component provided.`);
    if (!(target as any).WrappedComponent) throw new Error(`Can't disconnect component, component is not a connected component.`);

    return (target as any).WrappedComponent;
}

export default disconnect;