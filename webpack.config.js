const resolve     = require('path').resolve;
const webpack     = require("webpack");
const packageJson = require('./package.json');

module.exports = {
    devtool  : "source-map",
    target   : 'web',
    entry    : {
        ["test-utils-v" + packageJson.version]: "./src/index.ts"
    },
    externals: {
        "tslib"           : "window"
    },
    output   : {
        path          : resolve(__dirname, "dist"),
        filename      : "target/[name]-[chunkhash].js",
        library       : "TestUtils",
        libraryTarget : 'umd',
        umdNamedDefine: true
    },
    resolve  : {
        extensions: [".ts", ".tsx", ".js", ".jsx"],
        modules   : [
            "node_modules",
            resolve(__dirname, "src")
        ],
    },
    module   : {
        rules: [
            {
                test   : /\.tsx?$/,
                loaders: ["ts-loader"]
            }
        ]
    },
    plugins  : [
        new webpack.optimize.OccurrenceOrderPlugin(true),
        new webpack.optimize.UglifyJsPlugin({
            comments : false,
            sourceMap: true,
            compress : {
                warnings: false
            }
        }),
        new webpack.LoaderOptionsPlugin({
            minimize : true,
            debug    : false,
            sourceMap: true
        })
    ]
};